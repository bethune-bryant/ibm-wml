#!/bin/bash

# A script to install the IBM Watson Machine Learning distribution in `/sw` for
# arbitrary OLCF systems.
#
# Authors: M. Belhorn
#          B. Nelson

set -e

module purge

TARGET_HOST="${1:-}"
PKG="ibm-wml-ce"
POWERAI_VERSION="${2}"
PKGREL="${3:-}"
GCC_VERSION="${4:-4.8.5}"
PKG_VERSION="${POWERAI_VERSION}-${PKGREL}"
CONDAENV_NAME="${PKG}-${PKG_VERSION}"
ROOT_DIR="/sw/${TARGET_HOST}/$PKG"
export ANACONDA_DIR="$ROOT_DIR/anaconda-base"
CONDAENV_LOCATION="${ANACONDA_DIR}/envs/${CONDAENV_NAME}"
BUILD_DIR="/tmp/$USER/${PKG}-install"
MODULE_ROOT="$ROOT_DIR/modulefiles/Core"
MODULES_DIR="$MODULE_ROOT/$PKG"
DDL_LOCAL_CHANNEL="file:///sw/sources/ibm-wml-ce/conda-channel/"
CONDA_SCRIPT="${ANACONDA_DIR}/etc/profile.d/conda.sh"

# Check for required arguments
if [[ -z "${TARGET_HOST}" || -z "${POWERAI_VERSION}" || -z "${PKGREL}" ]]; then
  echo "Usage: ./install.sh HOST POWERAI_VERSION REV [GCC_VERSION]"
  echo "    where:"
  echo "       HOST in {'summit', 'peak', ...}"
  echo "       POWERAI_VERSION is the target version of PowerAI {1.6.0, 1.6.1, etc...}"
  echo "       REV is a unique suffix for the conda env and modulefile names"
  echo "       GCC_VERSION is the GCC toolchain to use for building packages via pip"
  [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

if [[ $POWERAI_VERSION == "1.7.0" ]]; then
  HOROVOD_COMMIT="NONE"
elif [[ $POWERAI_VERSION == "1.6.2" ]]; then
  HOROVOD_COMMIT="bbf09d79c257d6629d7366e0d33cf761914bc7f0"
elif [[ $POWERAI_VERSION == "1.6.1" ]]; then
  HOROVOD_COMMIT="9f87459ead9ebb7331e1cd9cf8e9a5543ecfb784"
else
  HOROVOD_COMMIT="master"
fi

echo "==> Installing IBM Watson Machine Learning distribution"
echo "    module:          $PKG"
echo "    module version:  $PKG_VERSION"
echo "    PowerAI version: $POWERAI_VERSION"
echo "    prefix:          $ROOT_DIR"
echo "    conda create -y --prefix ${CONDAENV_LOCATION} ${CHANNELS} ..."

echo "    WARNING: DO NOT RUN THIS SCRIPT UNATTENDED"
echo "             This script requires interactive input"
echo ""

read -p "Are the above values correct? (y/[n]) " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  # handle exits from shell or function but don't exit interactive shell
  [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

mkdir -p "$ROOT_DIR"
mkdir -p "$BUILD_DIR"
mkdir -p "$MODULES_DIR"
cd "${BUILD_DIR}"

echo "$(\date --iso-8601=seconds) - Installed using \`/sw/sources/ibm-wml-ce/install.sh $TARGET_HOST $POWERAI_VERSION\`." >> $ROOT_DIR/README.md

installer="Anaconda3-2019.07-Linux-ppc64le.sh"
[ ! -f "./${installer}" ] \
  && wget "https://repo.continuum.io/archive/${installer}" \
  && chmod a+x "${installer}"

echo "==> Preparing base installation"
${ANACONDA_DIR}/bin/python --version >/dev/null 2>/dev/null \
  || ./${installer} -bp "$ANACONDA_DIR"

echo "==> Generating modulefiles"
cat << EOF > "${MODULES_DIR}/${PKG_VERSION}.lua"
local root_dir = "$ROOT_DIR"
local prefix = "${ANACONDA_DIR}"

help([[
Description - Watson Machine Learning Community Edition
Docs - https://www.ibm.com/support/knowledgecenter/en/SS5SF7_${POWERAI_VERSION}

Included is a Summit specific distribution of DDL that supports up to 954 nodes and is
optimized for use on Summit.

A python anaconda environment that is pre-loaded with many popular machine learning frameworks
and tuned to Summit's Power9+Nvidia Volta hardware. Packages available include:

- TensorFlow (CPU and GPU optimized)
- IBM PowerAI
- snapml-spark
- caffe (GPU-enabled)
- ddl (Summit optimized)
- PyTorch
- Keras
- SciKit
- dask
- magma
- Jupyter

]])

-- Required for "module display ..."
whatis("Watson Machine Learning CE.")
whatis("Description: A python anaconda environment pre-loaded with many popular machine learning frameworks and tuned to Summit's Power9+Nvidia Volta hardware.")
whatis("Packages: tensorflow powerai snapml-spark caffe ddl pytorch keras scikit dask jupyter")

conflict('python')
load('spectrum-mpi', 'cuda')

-- prepend_path("PATH", pathJoin(prefix, "bin"))
setenv("OLCF_WML_ROOT", prefix)

function myShell()
  local shell = os.getenv('SHELL') or '/usr/bin/sh'
  local root, fname, suffix = string.match(shell, "(.-)([^\\\\/]-%.?([^%.\\\\/]*))$")
  return fname
end

if mode() == 'load' then
  local my_shell = myShell()
  if my_shell == "zsh" then
    -- Overload the zsh 'type' builtin (equal to 'whence -v') be compatible
    -- with bash-style 'type -P' invocations.
    execute {cmd='function type () { if [[ "\$@[1]" == "-P" ]]; then; whence -p "\${@:2}"; else; whence -v "\$@"; fi }', modeA={'load'}}
  elseif my_shell == "tcsh" or my_shell == "csh" then
    LmodMessage(myModuleUsrName() .. ":")
    LmodMessage("    Only bash/sh shells are officially supported by IBM WML CE.")
    LmodMessage("    The conda environment may not be fully initialized.")
  end
end

execute {cmd="source ${CONDA_SCRIPT} && conda activate ${CONDAENV_LOCATION}",modeA={"load"}}
execute {cmd="source ${CONDA_SCRIPT} && conda deactivate", modeA={"unload"}}
EOF

echo "==> Loading IBM WML environment"
module load gcc${GCC_VERSION:+/${GCC_VERSION}}
module load spectrum-mpi
module load cuda
echo ""
module -t list
echo ""
source ${CONDA_SCRIPT}

# Activate the base conda environment to put it's python binary in the PATH.
conda activate

#Update conda to 4.7
conda install -n base -c defaults conda=4.7

# Raise error and exit if python binary is wrong.
echo "==> Testing distribution location"
set +e
read -r -d '' _py_location_test <<'EOF'
import sys
import os
prefix = sys.prefix
expected = os.environ.get('ANACONDA_DIR', '$ANACONDA_DIR in environment')
print("==> Python binary found in\n    %s" % prefix)
try:
    assert prefix.startswith(expected)
except:
    print("!=> Fatal error: expected %s" % expected)
    sys.exit(1)
sys.exit(0)
EOF
set -e
python -c "$_py_location_test"

# Add IBM WML channels and lock default conda channel priority for the base
# installation.
echo "==> Configuring system-scope conda channels"
conda config \
  --system --add default_channels "https://repo.anaconda.com/pkgs/main/"
conda config \
  --system --add default_channels "https://repo.anaconda.com/pkgs/r/"
conda config \
  --system --prepend channels \
  "https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda/"
conda config \
  --system --prepend channels \
  "${DDL_LOCAL_CHANNEL}"

# Use these channel overrides to prevent conda from using the .condarc of the user
# running this install script
CHANNELS="--override-channels -c ${DDL_LOCAL_CHANNEL} -c https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda/ -c https://repo.anaconda.com/pkgs/main/ -c https://repo.anaconda.com/pkgs/r/ -c defaults"

# Make the conda commands a little more fault-tolerant
conda config --system --set remote_max_retries 5
conda config --system --set remote_connect_timeout_secs 20
conda config --system --set remote_read_timeout_secs 150

echo "==> Installing IBM WML-CE and other required packages in ${CONDAENV_NAME} conda environment"
export IBM_POWERAI_LICENSE_ACCEPT=yes

if [[ $POWERAI_VERSION == "1.6.2" || $POWERAI_VERSION == "1.6.1" ]]; then
    #If installing 1.6.1 or 1.6.2, use pip to install the the correct horovod commit.
    # Install all the IBM-WML-CE applications
    conda create -y --prefix ${CONDAENV_LOCATION} ${CHANNELS} \
    "python=3.6" \
    "powerai==${POWERAI_VERSION}" \
    "gxx_linux-ppc64le==7.3.0" \
    "cffi" \
    "cudatoolkit-dev" \
    "powerai-rapids" \
    "bazel" \
    "dali" \
    "keras-gpu" \
    "tensorflow-serving" \
    "tensorrt-samples" \
    "torchvision" \
    "magma"
else
    #From 1.7.0 onward, the conda channel will include horovod.
    # Install all the IBM-WML-CE applications
    echo "==> Installing IBM WML-CE and other required packages in ${CONDAENV_NAME} conda environment"
    export IBM_POWERAI_LICENSE_ACCEPT=yes
    conda create -y --prefix ${CONDAENV_LOCATION} ${CHANNELS} \
    "python=3.6" \
    "powerai==${POWERAI_VERSION}" \
    "cudatoolkit-compat" \
    "powerai-rapids" \
    "bazel" \
    "dali" \
    "keras-gpu" \
    "tensorflow-serving" \
    "tensorrt-samples" \
    "torchvision" \
    "magma" \
    "tensorflow-benchmarks" \
    "horovod"
fi

echo "==> Activating ${CONDAENV_NAME} conda environment"
conda activate ${CONDAENV_NAME}


echo "==> Verifying summit specific versions of packages were installed"
for PACKAGE in ddl powerai-license spectrum-mpi ; do
if [[ ! $(conda list ^${PACKAGE}$ | grep SUMMIT) ]]; then
    echo "!=> Fatal error: Installed ${PACKAGE} is not Summit version."
    # handle exits from shell or function but don't exit interactive shell
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi
done

if [[ $POWERAI_VERSION == "1.6.2" || $POWERAI_VERSION == "1.6.1" ]]; then
    #If installing 1.6.1 or 1.6.2, use pip to install the correct horovod commit.
    echo "==> Building and Installing Horovod with NCCL backend"
    HOROVOD_CUDA_HOME="${CONDA_PREFIX}" HOROVOD_GPU_ALLREDUCE=NCCL \
    HOROVOD_WITHOUT_GLOO=1 HOROVOD_WITHOUT_MXNET=1 \
    pip install --no-cache-dir \
    git+https://github.com/horovod/horovod.git@${HOROVOD_COMMIT}


    # Remove gxx to not interfere with the system gxx.
    conda remove -y ${CHANNELS} gxx_linux-ppc64le
fi

set +e
chmod -R go-w "${ANACONDA_DIR}"
set -e

echo "==> Updating public modulefile links"
ln -sni "${MODULES_DIR}" "/sw/${TARGET_HOST}/modulefiles/core/$PKG"

read -p "Do you wish to make the new environment module the default (y/[n]) " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
  echo "==> Updating default modulefile"
  [ -f "${MODULES_DIR}/${PKG_VERSION}.lua" ] \
      && ln -snf "${PKG_VERSION}.lua" ${MODULES_DIR}/default
fi
echo "==> Done!"
