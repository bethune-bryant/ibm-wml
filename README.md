# OLCF IBM Watson Machine Learning Installation Script

[This script](https://gitlab.com/mpbelhorn/ibm-wml/-/blob/master/install.sh) manages installations of the IBM Watson Machine Learning / PowerAI /
DDL anaconda distribution on OLCF Power9 systems. It exists to provide
transparency as to how the distribution is installed for OLCF software
maintainers.

The documentation for WML-CE on Summit can be found [here](https://docs.olcf.ornl.gov/software/analytics/ibm-wml-ce.html).

## Architecture

The general architecture is that we have a global, read-only anaconda installation, with conda environments for each module version, where each module version corresponds with a WML-CE release, and a Summit local conda channel for specialized build deployment. When a specific module version is loaded the anaconda activation scripts are sourced, and the corresponding conda environment is activated.

If a user wants different packages in the conda environment they are able to load one of the module versions and either clone an environment or create a whole new environment anywhere they have write access.

### Module Revisions

The deployment script accepts a revision number in addition to the WML-CE version. This is so that if we ever need to re-deploy a WML-CE version, for whatever reason, we can keep the previous deployment accessible. This is really important for any users wanting to be able to repeat experiments in the future.

## Local Conda Channel

The ORNL contract required some specially built packages, so we deployed those in a Summit internal conda channel. You can see [here](https://gitlab.com/mpbelhorn/ibm-wml/-/blob/master/install.sh#L193) where the installation script adds the local channel so that all users that load the module will use it.

Another reason we used the Summit internal conda channel is so that we could provide a Spectrum MPI stub package. IBM WML-CE ships an SMPI conda package and it's a dependency of many of the packages. Since Summit has a system-wide installation of SMPI, we wanted to make sure that users are using the system SMPI instead of our conda version. The stub package we provide in the local channel satisfies the dependencies while not overwriting the system SMPI. This lets users install packages that depend on SMPI but still use the system SMPI. This happens automatically without users needing to be aware that it's happening.

[This section of the script](https://gitlab.com/mpbelhorn/ibm-wml/-/blob/master/install.sh#L249) ensures summit specific builds of packages are getting pulled from the local channel.

### Making the Local Channel

The SMPI stub package can be built with the recipe [here](https://gitlab.com/mpbelhorn/ibm-wml/-/blob/master/smpi-stub-pkg).

You should be able change the build string to what you want and run `conda build --no-include-recipe --output-folder ~/conda-bld ~/recipe`. You can see this is just an empty recipe that satisfies the SMPI dependency without changing anything.

`conda build` will make a channel, but you can also make a channel by putting the build in a globally readable location within a directory structure like `..../local_channel/linux-ppc64le/spectrum-mpi-10.03-64_SUMMIT.g3947c96.tar.bz2` next, navigate to `..../local_channel/` and run `conda index`. You can find more info about setting up a local channel [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/create-custom-channels.html).

## Access Permissions

It's good practice to ensure that the anaconda installation is read-only for everyone, including maintainers. Many conda commands unknowingly make changes to the environment which would affect all users. Write access should only be used for the deployment script.
